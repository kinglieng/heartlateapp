import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:hartrate/users/pages/home_page.dart';
import 'package:hartrate/users/pages/login/login_page.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_line_sdk/flutter_line_sdk.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  // await Firebase.initializeApp(
  //   options: const FirebaseOptions(
  //       apiKey: "AIzaSyB7AwzKU_VKkhB_iNouSBn_3KFYrDrNoEg",
  //       authDomain: "hartlate-app.firebaseapp.com",
  //       projectId: "hartlate-app",
  //       storageBucket: "hartlate-app.appspot.com",
  //       messagingSenderId: "59443078435",
  //       appId: "1:59443078435:web:919f8b63eff93f8472d896",
  //       measurementId: "G-FCQ78YNVBM"),
  // );

  await dotenv.load(fileName: ".env");
  final channal = dotenv.env['your_channel_id'];
  await LineSDK.instance.setup("${channal}").then((_) {
    print('LineSDK Prepared');
  });

  runApp(const MyApp());
  configLoading();
}

void configLoading() {
  EasyLoading.instance
    ..maskType = EasyLoadingMaskType.black
    ..userInteractions = false
    ..dismissOnTap = false;
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (context) => MyHomePage(),
        '/login': (context) => Login_Page(),
      },
      debugShowCheckedModeBanner: false,
      builder: EasyLoading.init(),
    );
  }
}
