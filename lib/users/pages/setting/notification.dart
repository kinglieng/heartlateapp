import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class NotificationPage extends StatelessWidget {
  final String channelAccessToken =
      'your_channel_access_token'; // แทนด้วย Channel Access Token ของคุณ
  final String userUid = 'user_uid'; // แทนด้วย UID ของผู้ใช้ Line
  final String messageText = 'ข้อความแจ้งเตือนของคุณ';

  Future<void> sendLineNotification() async {
    final url = Uri.parse('https://api.line.me/v2/bot/message/push');

    final headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $channelAccessToken',
    };

    final data = {
      'to': userUid,
      'messages': [
        {
          'type': 'text',
          'text': messageText,
        },
      ],
    };

    final response = await http.post(
      url,
      headers: headers,
      body: jsonEncode(data),
    );

    if (response.statusCode == 200) {
      print('ส่งข้อความ Line แจ้งเตือนสำเร็จ');
    } else {
      print(
          'เกิดข้อผิดพลาดในการส่งข้อความ Line แจ้งเตือน: ${response.statusCode}');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ส่งข้อความ Line แจ้งเตือน'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: sendLineNotification,
          child: Text('ส่งข้อความ Line แจ้งเตือน'),
        ),
      ),
    );
  }
}
