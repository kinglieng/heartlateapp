import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_line_sdk/flutter_line_sdk.dart';
import 'package:hartrate/widget/option.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';

class LineLoginPage extends StatefulWidget {
  @override
  _LineLoginPageState createState() => _LineLoginPageState();
}

class _LineLoginPageState extends State<LineLoginPage> {
  Future<void> loginWithLine() async {
    try {
      final result =
          await LineSDK.instance.login(scopes: ["profile", "openid"]);

      if (result != null) {
        await CreateUserLineFirebase(result);
        var accesstoken = await getAccessToken();
        var displayname = result.userProfile?.displayName;
        var statusmessage = result.userProfile?.statusMessage;
        var imgUrl = result.userProfile?.pictureUrl;
        var userId = result.userProfile?.userId;
        print("AccessToken> " + accesstoken!);
        print("DisplayName> " + displayname!);
        print("StatusMessage> " + statusmessage!);
        print("ProfileURL> " + imgUrl!);
        print("userId> " + userId!);
      }
      print(result.toString());
    } on PlatformException catch (e) {
      switch (e.code.toString()) {
        case 'CANCEL':
          _dialogBuilder(context, "คุณยกเลิกการเข้าสู่ระบบ");
          break;
        case 'AUTHENTICATION_AGENT_ERROR':
          _dialogBuilder(context, "คุณไม่อนุญาติการเข้าสู่ระบบด้วย LINE");
          break;
        default:
          _dialogBuilder(context, "เกิดข้อผิดพลาด");
      }
      print("Line login error: $e");
    }
  }

  Future<void> CreateUserLineFirebase(LoginResult result) async {
    try {
      final firebaseUser = FirebaseAuth.instance.currentUser;

      if (firebaseUser == null) {
        // สร้างบัญชี Firebase Authentication

        // อัพเดตข้อมูลของผู้ใช้ใน Firebase Firestore หรือ Realtime Database
        // โดยใช้ข้อมูลจาก result.userProfile
      } else {
        // อัพเดตข้อมูลของผู้ใช้ใน Firebase Firestore หรือ Realtime Database
        // โดยใช้ข้อมูลจาก result.userProfile
      }
    } catch (e) {
      print("Firebase Authentication error: $e");
    }
  }

  Future<void> _dialogBuilder(BuildContext context, String text) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('แจ้งเตือน!!!'),
          content: Text("${text}"),
          actions: <Widget>[
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: const Text('ตกลง'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future getAccessToken() async {
    try {
      final result = await LineSDK.instance.currentAccessToken;
      return result?.value;
    } on PlatformException catch (e) {
      print(e.message);
      switch (e.code.toString()) {
        case 'CANCEL':
          _dialogBuilder(context, "คุณยกเลิกการเข้าสู่ระบบ");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Line Login Page'),
      ),
      body: Center(
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              padding: EdgeInsets.all(1),
              backgroundColor: Color.fromRGBO(0, 185, 0, 1),
              fixedSize: const Size(250, 50),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              )),
          onPressed: loginWithLine,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Image.network(
                'https://firebasestorage.googleapis.com/v0/b/messagingapitutorial.appspot.com/o/line_logo.png?alt=media&token=80b41ee6-9d77-45da-9744-2033e15f52b2',
                width: 50,
                height: 50,
              ),
              sizedBoxWidth(20),
              Text(
                'เข้าสู่ระบบด้วย LINE',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
