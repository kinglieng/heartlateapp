import 'package:flutter/material.dart';
import 'package:hartrate/users/pages/login/login_page.dart';
import 'dart:async';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _showImage = false; // ตัวแปรสำหรับควบคุมการแสดงภาพ
  bool skipPage = false;

  Future<void> _navigateToLoginPage() async {
    await Future.delayed(const Duration(seconds: 5));
    if (!skipPage) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => Login_Page(),
        ),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    // เริ่มทำงานของอนิเมชัน
    Timer.periodic(const Duration(seconds: 2), (Timer timer) {
      setState(() {
        _showImage = !_showImage; // แสดงภาพหลังจากผ่านเวลา 2 วินาที
      });
    });

    _navigateToLoginPage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage(
              'images/backgroundapp.png',
            ),
            fit: BoxFit.cover, // ปรับขนาดรูปภาพให้เต็มพื้นที่
          ),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                ' HeartRate App',
                style: TextStyle(
                    fontFamily: 'Kanit ',
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Color.fromARGB(225, 255, 60, 0)),
              ),
              const SizedBox(height: 50),
              // AnimatedSwitcher จะใช้งานเอฟเฟกต์อนิเมชันให้กับวัตถุต่าง ๆ
              AnimatedSwitcher(
                duration: const Duration(seconds: 1),
                child: _showImage
                    ? Image.asset(
                        'images/heartrate.png',
                        key: const ValueKey<bool>(
                            true), // คีย์ตัวแปรเพื่อให้ AnimatedSwitcher รู้ว่าเปลี่ยนเรื่อง
                        width: 200,
                        height: 200,
                      )
                    : Image.asset(
                        'images/heartratered.png',
                        key: const ValueKey<bool>(
                            false), // คีย์ตัวแปรเพื่อให้ AnimatedSwitcher รู้ว่าเปลี่ยนเรื่อง
                        width: 200,
                        height: 200,
                      ),
              ),
              const SizedBox(height: 100),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: const Color.fromARGB(210, 249, 83, 71),
                    foregroundColor: Colors.white,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 30, vertical: 15),
                    fixedSize: const Size(200, 50),
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(25),
                      ),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      skipPage = true;
                    });
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Login_Page(),
                      ),
                    );
                  },
                  child: const Text('START',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold)))
            ],
          ),
        ),
      ),
    );
  }
}
