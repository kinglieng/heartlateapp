import 'dart:async';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hartrate/services/user_data_service.dart';
import 'package:hartrate/widget/option.dart';
import 'package:location/location.dart';
import 'package:text_form_field_wrapper/text_form_field_wrapper.dart';

class ModePatients extends StatefulWidget {
  final Stream<int> heartRateStream;
  const ModePatients({
    Key? key,
    required this.heartRateStream,
  });

  @override
  State<ModePatients> createState() => _ModePatientsState();
}

Completer<GoogleMapController> _controller = Completer();

class _ModePatientsState extends State<ModePatients> {
  StreamController<int> _heartRateController =
      StreamController<int>.broadcast();
  StreamSubscription<int>? heartRateSubscription;
  User? user = FirebaseAuth.instance.currentUser;
  LocationData? currentLocation;
  final TextEditingController nameController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController addressController = TextEditingController();
  final TextEditingController relativeNameController = TextEditingController();
  final TextEditingController relativePhoneController = TextEditingController();
  final TextEditingController diseaseController = TextEditingController();
  bool hasPatientHistory = false; // Track if patient history exists
  int currentHeartRate = 0;

  @override
  void initState() {
    super.initState();
    checkPatientHistory();
  }

  Future<void> checkPatientHistory() async {
    if (user != null) {
      final List<Map<String, dynamic>> patientHistoryData =
          await PatientDataService.fetchPatientHistoryData();

      // Check if patient history data exists
      if (patientHistoryData.isNotEmpty) {
        // Patient history exists, populate the fields
        final patientData = patientHistoryData[0];
        nameController.text = patientData['name'];
        phoneController.text = patientData['phone'];
        addressController.text = patientData['address'];
        relativeNameController.text = patientData['relative_name'];
        relativePhoneController.text = patientData['relative_phone'];
        diseaseController.text = patientData['disease'];

        // Set hasPatientHistory to true
        setState(() {
          hasPatientHistory = true;
        });
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  void saveChanges() async {
    try {
      if (user != null) {
        await FirebaseFirestore.instance
            .collection('users')
            .doc(user!.uid)
            .collection('patient_history')
            .add({
          'name': nameController.text,
          'phone': phoneController.text,
          'address': addressController.text,
          'relative_name': relativeNameController.text,
          'relative_phone': relativePhoneController.text,
          'disease': diseaseController.text,
        });
        print('บันทึกประวัติผู้ป่วยสำเร็จ');
      }
    } catch (e) {
      print("errorMessage = $e");
    }
  }

  Future<void> _showPatientHistoryDialog() async {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('กรอกประวัติผู้ป่วย'),
          content: SingleChildScrollView(
            child: Form(
              child: Column(
                children: <Widget>[
                  TextFormFieldWrapper(
                    formField: TextFormField(
                      controller: nameController,
                      decoration: InputDecoration(
                          labelText: 'ชื่อ-นามสกุล', border: InputBorder.none),
                    ),
                  ),
                  sizedBoxHeight(10),
                  TextFormFieldWrapper(
                    formField: TextFormField(
                      controller: phoneController,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          labelText: 'เบอร์โทร', border: InputBorder.none),
                    ),
                  ),
                  sizedBoxHeight(10),
                  TextFormFieldWrapper(
                    formField: TextFormField(
                      controller: addressController,
                      decoration: InputDecoration(
                          labelText: 'ที่อยู่', border: InputBorder.none),
                    ),
                  ),
                  sizedBoxHeight(10),
                  TextFormFieldWrapper(
                    formField: TextFormField(
                      controller: diseaseController,
                      decoration: InputDecoration(
                          labelText: 'โรคประจำตัว', border: InputBorder.none),
                    ),
                  ),
                  sizedBoxHeight(10),
                  Row(
                    children: [
                      Expanded(
                        child: TextFormFieldWrapper(
                          formField: TextFormField(
                            controller: relativeNameController,
                            decoration: InputDecoration(
                                labelText: 'ชื่อญาติ',
                                border: InputBorder.none),
                          ),
                        ),
                      ),
                      sizedBoxWidth(10),
                      Expanded(
                        child: TextFormFieldWrapper(
                          formField: TextFormField(
                            controller: relativePhoneController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: 'เบอร์โทรญาติ',
                                border: InputBorder.none),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('ยกเลิก'),
            ),
            TextButton(
              onPressed: () {
                saveChanges();
                Navigator.of(context).pop();
              },
              child: Text('บันทึก'),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'โหมดผู้ป่วย',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        body: StreamBuilder<int>(
          stream: widget.heartRateStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              currentHeartRate = snapshot.data!;
            }
            return Container(
              width: double.infinity,
              margin: EdgeInsets.all(20),
              child: Column(
                children: [
                  Column(
                    children: [
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Color.fromARGB(255, 4, 80, 110),
                              fixedSize: const Size(150, 50),
                              shadowColor: Colors.black,
                              elevation: 5,
                              shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20)))),
                          onPressed: () {},
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const SpinKitPumpingHeart(
                                color: Color.fromARGB(255, 255, 17, 0),
                                size: 20,
                              ),
                              sizedBoxWidth(8),
                              Text(
                                '$currentHeartRate BPM',
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              ),
                            ],
                          )),
                    ],
                  ),
                  sizedBoxHeight(50),
                  ElevatedButton(
                    onPressed: _showPatientHistoryDialog,
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Color.fromARGB(238, 33, 79, 163),
                      fixedSize: const Size(250, 50),
                      shadowColor: Colors.black,
                      elevation: 5,
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10),
                        ),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.assignment_add,
                          color: Colors.white,
                        ),
                        sizedBoxWidth(10),
                        Text(
                          hasPatientHistory
                              ? 'แสดงประวัติผู้ป่วย'
                              : 'กรอกประวัติผู้ป่วย',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          },
        ));
  }
}
