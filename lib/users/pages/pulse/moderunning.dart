// ignore_for_file: avoid_print

import 'dart:async';
import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:hartrate/widget/option.dart';
import 'package:hartrate/widget/textrunner.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'dart:ui';
import '../../../services/user_data_service.dart';
import 'package:carousel_slider/carousel_slider.dart';

class ModeRunner extends StatefulWidget {
  final Stream<int> heartRateStream;

  // final int? bpmValue;
  ModeRunner({
    Key? key,
    required this.heartRateStream,
  });

  @override
  State<ModeRunner> createState() => _ModeRunnerState();
}

class _ModeRunnerState extends State<ModeRunner> {
  StreamController<int> _heartRateController =
      StreamController<int>.broadcast();
  StreamSubscription<int>? heartRateSubscription;
  TextEditingController nameController = TextEditingController();
  TextEditingController ageController = TextEditingController();
  int gendervalue = 0;
  int currentHeartRate = 0;
  int ageValue = 0;
  int elapsedTime = 0;
  double percent = 0.0;
  List<double> zonePercent = [0.0, 0.0, 0.0, 0.0, 0.0];
  Timer? periodicBpmTimer;
  Future<void> fetchUserData() async {
    final fetchedUserData = await UserDataService.fetchUserData();
    setState(() {
      nameController.text = fetchedUserData['name'];
      ageController.text = fetchedUserData['age'];
      gendervalue = fetchedUserData['gender'];
      ageValue = int.parse(fetchedUserData['age']);
    });
  }

  double MaxHR(int value) {
    if (value == 0) {
      return 214 - (0.8 * ageValue);
    } else if (value == 1) {
      return 159 - (0.7 * ageValue);
    } else {
      return 0.0;
    }
  }

  void incrementPercentForZone(int zoneIndex) {
    setState(() {
      if (zonePercent[zoneIndex] < 10.0) {
        zonePercent[zoneIndex] += 0.01; // เพิ่มค่า percent ทีละ 0.01
      } else {
        zonePercent[zoneIndex] =
            10.0; // ถ้า percent เกิน 1.0 ให้กำหนดให้เป็น 1.0s
      }
    });
  }

  @override
  void initState() {
    super.initState();
    fetchUserData();
    heartRateSubscription = widget.heartRateStream.listen((int heartRate) {
      setState(() {
        currentHeartRate = heartRate;
      });
      if (periodicBpmTimer == null || !periodicBpmTimer!.isActive) {
        periodicBpmTimer = Timer.periodic(Duration(seconds: 10), (timer) {
          elapsedTime = timer.tick * 10;
          if (currentHeartRate >= 0.9 * MaxHR(gendervalue)) {
            incrementPercentForZone(4);
          }

          // ตรวจสอบว่า currentHeartRate อยู่ในโซน 4
          if (currentHeartRate >= 0.8 * MaxHR(gendervalue)) {
            incrementPercentForZone(3);
          }

          // ตรวจสอบว่า currentHeartRate อยู่ในโซน 3
          if (currentHeartRate >= 0.7 * MaxHR(gendervalue)) {
            incrementPercentForZone(2);
          }

          // ตรวจสอบว่า currentHeartRate อยู่ในโซน 2
          if (currentHeartRate >= 0.6 * MaxHR(gendervalue)) {
            print('${currentHeartRate}');
            incrementPercentForZone(1);
          }

          // ตรวจสอบว่า currentHeartRate อยู่ในโซน 1
          if (currentHeartRate >= 0.5 * MaxHR(gendervalue)) {
            incrementPercentForZone(0);
          }

          print(
              "Time in Zone 1: ${calculateTimeInZone(zonePercent[0], MaxHR(gendervalue), elapsedTime)}");
          print(
              "Time in Zone 2: ${calculateTimeInZone(zonePercent[1], MaxHR(gendervalue), elapsedTime)}");
          print(
              "Time in Zone 3: ${calculateTimeInZone(zonePercent[2], MaxHR(gendervalue), elapsedTime)}");
          print(
              "Time in Zone 4: ${calculateTimeInZone(zonePercent[3], MaxHR(gendervalue), elapsedTime)}");
          print(
              "Time in Zone 5: ${calculateTimeInZone(zonePercent[4], MaxHR(gendervalue), elapsedTime)}");
        });
      }
    });
  }

  String calculateTimeInZone(double percent, double maxHR, int elapsedTime) {
    int totalSeconds = (percent * maxHR * 0.01 * elapsedTime).toInt();
    int minutes = totalSeconds ~/ 60;
    //int seconds = totalSeconds % 60;
    return "${minutes.toString()} ";
  }

  @override
  void dispose() {
    periodicBpmTimer?.cancel();
    heartRateSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Mode Runner'),
          backgroundColor: const Color.fromARGB(255, 29, 26, 26),
        ),
        body: SingleChildScrollView(
          child: StreamBuilder<int>(
            stream: widget.heartRateStream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                currentHeartRate = snapshot.data!;
              }
              return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  sizedBoxHeight(20),
                  Text(
                    'Max HeartRate ของคุณ = ${MaxHR(gendervalue)}',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  sizedBoxHeight(10),
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                    Text(
                      'BPM Value : ${currentHeartRate}',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    sizedBoxWidth(10),
                    const SpinKitPumpingHeart(
                      color: Colors.red,
                      size: 20,
                    ),
                  ]),
                  sizedBoxHeight(15),
                  CarouselSlider(
                    items: [
                      Container(
                        color: const Color.fromARGB(66, 255, 214, 214),
                        child: Card(
                          elevation: 5,
                          margin: EdgeInsets.all(8),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: <Widget>[
                                TextMaxHeartRate(),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: Card(
                          margin: EdgeInsets.all(5),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(children: [
                              TextModeMHR(),
                            ]),
                          ),
                        ),
                        decoration: BoxDecoration(boxShadow: [
                          BoxShadow(
                            color: Colors.blue,
                            blurRadius: 10.0,
                          ),
                        ]),
                      ),
                    ],
                    options: CarouselOptions(
                      height: 220,
                      aspectRatio: 16 / 10,
                      initialPage: 0,
                      enableInfiniteScroll: true,
                      reverse: false,
                      autoPlay: true,
                      autoPlayInterval: Duration(seconds: 20),
                      autoPlayAnimationDuration: Duration(milliseconds: 3000),
                      autoPlayCurve: Curves.fastOutSlowIn,
                      enlargeCenterPage: true,
                      enlargeFactor: 0.3,
                      scrollDirection: Axis.horizontal,
                    ),
                  ),
                  sizedBoxHeight(20),
                  Text(
                    'เวลาในโซนอัตราเต้นหัวใจ',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                  sizedBoxHeight(10),
                  Container(
                    width: 350,
                    decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(15.0)),
                    child: Column(
                      children: [
                        sizedBoxHeight(3),
                        Text(
                          'Zone 5 > ${(0.9 * MaxHR(gendervalue)).toInt()} - Max HR',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              LinearPercentIndicator(
                                width: MediaQuery.of(context).size.width - 100,
                                animation: true,
                                lineHeight: 15,
                                percent: zonePercent[4],
                                center: Text(
                                  "${(zonePercent[4] * 100).toStringAsFixed(1)}%",
                                  style: TextStyle(fontSize: 14),
                                ),
                                progressColor: Colors.red,
                                barRadius: const Radius.circular(10),
                              ),
                              Text(
                                " ${calculateTimeInZone(zonePercent[4], MaxHR(gendervalue), elapsedTime)}",
                                style: TextStyle(fontSize: 14),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  sizedBoxHeight(10),
                  Container(
                    width: 350,
                    decoration: BoxDecoration(
                        color: Colors.orangeAccent,
                        borderRadius: BorderRadius.circular(15.0)),
                    child: Column(
                      children: [
                        sizedBoxHeight(3),
                        Text(
                            'Zone 4 > ${(0.8 * MaxHR(gendervalue)).toInt()} - ${(0.9 * MaxHR(gendervalue)).toInt()}',
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              LinearPercentIndicator(
                                width: MediaQuery.of(context).size.width - 100,
                                animation: true,
                                lineHeight: 15,
                                percent: zonePercent[3],
                                center: Text(
                                  "${(zonePercent[3] * 100).toStringAsFixed(1)}%",
                                  style: TextStyle(fontSize: 12),
                                ),
                                progressColor: Colors.orange,
                                barRadius: const Radius.circular(10),
                              ),
                              Text(
                                " ${calculateTimeInZone(zonePercent[3], MaxHR(gendervalue), elapsedTime)}",
                                style: TextStyle(fontSize: 14),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  sizedBoxHeight(10),
                  Container(
                    width: 350,
                    decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.circular(15.0)),
                    child: Column(
                      children: [
                        sizedBoxHeight(3),
                        Text(
                          'Zone 3 > ${(0.7 * MaxHR(gendervalue)).toInt()} - ${(0.8 * MaxHR(gendervalue)).toInt()}',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              LinearPercentIndicator(
                                width: MediaQuery.of(context).size.width - 100,
                                animation: true,
                                lineHeight: 15,
                                percent: zonePercent[2],
                                center: Text(
                                  "${(zonePercent[2] * 100).toStringAsFixed(1)}%",
                                  style: TextStyle(fontSize: 14),
                                ),
                                progressColor: Colors.green,
                                barRadius: const Radius.circular(10),
                              ),
                              Text(
                                " ${calculateTimeInZone(zonePercent[2], MaxHR(gendervalue), elapsedTime)}",
                                style: TextStyle(fontSize: 14),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  sizedBoxHeight(10),
                  Container(
                    width: 350,
                    decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(15.0)),
                    child: Column(
                      children: [
                        sizedBoxHeight(3),
                        Text(
                            'Zone 2 > ${(0.6 * MaxHR(gendervalue)).toInt()} - ${(0.7 * MaxHR(gendervalue)).toInt()}',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold)),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              LinearPercentIndicator(
                                width: MediaQuery.of(context).size.width - 100,
                                animation: true,
                                lineHeight: 15,
                                percent: zonePercent[1],
                                center: Text(
                                  "${(zonePercent[1] * 100).toStringAsFixed(1)}%",
                                  style: TextStyle(fontSize: 14),
                                ),
                                progressColor: Colors.blue,
                                barRadius: const Radius.circular(10),
                              ),
                              Text(
                                " ${calculateTimeInZone(zonePercent[1], MaxHR(gendervalue), elapsedTime)}",
                                style: TextStyle(fontSize: 14),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  sizedBoxHeight(10),
                  Container(
                    width: 350,
                    decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(15.0)),
                    child: Column(
                      children: [
                        sizedBoxHeight(3),
                        Text(
                          'Zone 1 > ${(0.5 * MaxHR(gendervalue)).toInt()} - ${(0.6 * MaxHR(gendervalue)).toInt()}',
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              LinearPercentIndicator(
                                width: MediaQuery.of(context).size.width - 100,
                                animation: true,
                                lineHeight: 15,
                                percent: zonePercent[0],
                                center: Text(
                                  "${(zonePercent[0] * 100).toStringAsFixed(1)}%",
                                  style: TextStyle(fontSize: 14),
                                ),
                                progressColor:
                                    Color.fromARGB(197, 248, 247, 247),
                                barRadius: const Radius.circular(10),
                              ),
                              Text(
                                " ${calculateTimeInZone(zonePercent[0], MaxHR(gendervalue), elapsedTime)}",
                                style: TextStyle(fontSize: 14),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              );
            },
          ),
        ));
  }
}
