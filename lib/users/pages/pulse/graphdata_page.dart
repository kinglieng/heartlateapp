import 'dart:async';
import 'dart:ui';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_reactive_ble/flutter_reactive_ble.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:hartrate/services/user_data_service.dart';
import 'package:hartrate/users/pages/pulse/modepatients.dart';
import 'package:hartrate/widget/option.dart';
import '../../../services/bluetooth_service.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import '../../../services/bpm_service.dart';
import 'moderunning.dart';
import 'package:cupertino_battery_indicator/cupertino_battery_indicator.dart';

class GraphData extends StatefulWidget {
  const GraphData({super.key});

  @override
  State<GraphData> createState() => _GraphDataState();
}

class BpmManager {
  int bpmThreshold;
  int timesave;

  BpmManager({required this.bpmThreshold, required this.timesave});

  void updateThreshold(int newThreshold) {
    bpmThreshold = newThreshold;
  }

  void updateTimeSave(int newTime) {
    timesave = newTime;
  }
}

class _GraphDataState extends State<GraphData> {
  final flutterReactiveBle = FlutterReactiveBle();

  StreamController<int> heartRateController = StreamController<int>.broadcast();
  StreamController<int> _heartRateStreamController =
      StreamController<int>.broadcast();
  StreamSubscription<int>? heartRateSubscription;
  bool isConnected = false;
  User? user = FirebaseAuth.instance.currentUser;
  TextEditingController nameController = TextEditingController();
  TextEditingController ageController = TextEditingController();
  int ageValue = 0;
  int maxHR = 0;
  int gendervalue = 0;
  int battery = 0;
  TextEditingController heightController = TextEditingController();
  TextEditingController weightController = TextEditingController();
  TextEditingController genderController = TextEditingController();
  final BluetoothServiceBle bluetoothServiceble = BluetoothServiceBle();
  bool isBluetoothOn = false;
  int currentHeartRate = 0;
  var bpmManager = BpmManager(bpmThreshold: 170, timesave: 1);
  final BpmService bpmService = BpmService();
  Timer? periodicBpmTimer; // Timer for periodic storage

  // Timer for threshold-based storage
  @override
  void initState() {
    super.initState();
    FlutterBluePlus.turnOn();
    fetchUserData();
  }

  void startBpmMonitoring() async {
    try {
      await bluetoothServiceble.connectToDevice(heartRateController);
      setState(() {
        isConnected = true;
      });
      final batteryStatus = await bluetoothServiceble.checkBatteryStatus();
      setState(() {
        battery = batteryStatus!;
      });
      print("$battery");
      heartRateSubscription = heartRateController.stream.listen((heartRate) {
        setState(() {
          currentHeartRate = heartRate;
        });
        _heartRateStreamController.add(currentHeartRate);

        if (periodicBpmTimer == null || !periodicBpmTimer!.isActive) {
          periodicBpmTimer =
              Timer.periodic(Duration(minutes: bpmManager.timesave), (timer) {
            bpmService.startBpmRecording(currentHeartRate, maxHR);
            print('Stored currentHeartRate: $currentHeartRate');
          });
        }

        if (currentHeartRate > bpmManager.bpmThreshold) {
          bpmService.startBpmRecording(currentHeartRate, maxHR);
          print(
              'Stored currentHeartRate (Threshold Exceeded): $currentHeartRate');
        }
      });
    } catch (e) {
      print('Error: $e');
    }
  }

  void stopBpmMonitoring() {
    bluetoothServiceble.disconnect();
    setState(() {
      isConnected = false;
      currentHeartRate = 0;
      periodicBpmTimer?.cancel(); // รีเซ็ตค่า bpm ปัจจุบันเมื่อหยุดตรวจวัด
    });
  }

  void updateCurrentHeartRate(int newHeartRate) {
    setState(() {
      currentHeartRate = newHeartRate;
    });
  }

  Future<void> fetchUserData() async {
    final fetchedUserData = await UserDataService.fetchUserData();
    final fetchedAge = int.parse(fetchedUserData['age']);
    final maxHeartRate = 220 - fetchedAge;
    setState(() {
      nameController.text = fetchedUserData['name'];
      ageController.text = fetchedUserData['age'];
      gendervalue = fetchedUserData['gender'];
      ageValue = fetchedAge;
      maxHR = maxHeartRate;
    });
  }

  void toggleConnection(isConnected) {
    if (isConnected) {
      stopBpmMonitoring();
    } else {
      startBpmMonitoring();
    }
    setState(() {});
  }

  @override
  void dispose() {
    bluetoothServiceble.disconnect();
    heartRateController.close();
    heartRateSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Heart Rate Monitor',
          style: TextStyle(),
        ),
        backgroundColor: const Color.fromARGB(255, 29, 26, 26),
      ),
      body: SingleChildScrollView(
        child: Container(
          constraints: BoxConstraints(
            minHeight: 800,
            minWidth: 500,
          ),
          width: double.infinity,
          color: const Color.fromARGB(207, 0, 0, 0),
          child: Column(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  sizedBoxHeight(40),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Container(
                      decoration: BoxDecoration(
                        gradient: const LinearGradient(colors: [
                          Color.fromARGB(255, 221, 221, 221),
                          Color.fromARGB(255, 198, 214, 240)
                        ]),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Column(
                        //1
                        children: [
                          sizedBoxHeight(10),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              sizedBoxWidth(20),
                              Padding(
                                padding: const EdgeInsets.only(right: 10.0),
                                child: BatteryIndicator(
                                  trackHeight: 15,
                                  value: battery.toDouble() / 100,
                                  iconOutline: Colors.green,
                                  iconOutlineBlur: 1.0,
                                  barColor: Colors.blue,
                                ),
                              ),
                              Text('${battery}%'),
                              sizedBoxWidth(50),
                              const SpinKitPumpingHeart(
                                color: Colors.red,
                                size: 50,
                              ),
                              sizedBoxWidth(10),
                              const Text(
                                "Pulse",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          sizedBoxHeight(10),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              sizedBoxWidth(20),
                              CircularPercentIndicator(
                                radius: 60.0,
                                lineWidth: 10.0,
                                percent: currentHeartRate / maxHR,
                                center: Text(
                                  '$currentHeartRate bpm ',
                                  style: const TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.bold),
                                ),
                                progressColor: currentHeartRate >= 0.9 * maxHR
                                    ? const Color.fromARGB(255, 255, 17, 0)
                                    : currentHeartRate >= 0.8 * maxHR
                                        ? Colors.red
                                        : currentHeartRate >= 0.7 * maxHR
                                            ? Colors.yellow
                                            : currentHeartRate >= 0.6 * maxHR
                                                ? Colors.blue
                                                : currentHeartRate >=
                                                        0.5 * maxHR
                                                    ? Colors.green
                                                    : const Color.fromARGB(
                                                        255, 143, 255, 115),
                              ),
                              sizedBoxWidth(30),
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.orange,
                                  fixedSize: const Size(140, 50),
                                  shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(20),
                                    ),
                                  ),
                                ),
                                onPressed: () async {
                                  toggleConnection(isConnected);
                                },
                                child: Text(
                                  isConnected ? "ปิดการเชื่อมต่อ" : "เชื่อมต่อ",
                                  style: const TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                          sizedBoxHeight(20),
                        ],
                      ),
                    ),
                  ),
                  sizedBoxHeight(30),
                  const Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'ตั้งค่าบันทึกข้อมูล อัตราการเต้นหัวใจ',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    ],
                  ),
                  sizedBoxHeight(10),
                  Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: 120,
                            height: 40,
                            child: Stack(
                              children: [
                                TextField(
                                  decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                          color:
                                              Color.fromARGB(255, 5, 113, 255),
                                        ),
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      hintText:
                                          '> ${bpmManager.bpmThreshold} bpm'),
                                  keyboardType: TextInputType.number,
                                  onChanged: (value) {
                                    final newThreshold = int.tryParse(value);
                                    if (newThreshold != null) {
                                      bpmManager.updateThreshold(newThreshold);
                                    }
                                  },
                                ),
                              ],
                            ),
                          ),
                          sizedBoxWidth(20),
                          Container(
                            width: 120,
                            height: 40,
                            child: Stack(
                              children: [
                                TextField(
                                  decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                          color:
                                              Color.fromARGB(255, 5, 113, 255),
                                        ),
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      hintText: '${bpmManager.timesave} นาที'),
                                  keyboardType: TextInputType.number,
                                  onChanged: (value) {
                                    final newTime = int.tryParse(value);
                                    if (newTime != null) {
                                      bpmManager.updateTimeSave(newTime);
                                    }
                                  },
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  sizedBoxHeight(30),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ModeRunner(
                                  heartRateStream: _heartRateStreamController
                                      .stream, // ส่ง Stream มาให้ ModeRunner
                                )),
                      );
                    },
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              width: 300,
                              height: 120,
                              decoration: BoxDecoration(
                                gradient: const LinearGradient(colors: [
                                  Color.fromARGB(255, 235, 224, 224),
                                  Color.fromARGB(255, 240, 198, 219)
                                ]),
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  sizedBoxWidth(15),
                                  const Text(
                                    "โหมดสำหรับวิ่งออกกำลังกาย",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  sizedBoxWidth(10),
                                  const Icon(
                                    Icons.directions_run,
                                    size: 30,
                                    color: Colors.black,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  sizedBoxHeight(30),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ModePatients(
                                  heartRateStream: _heartRateStreamController
                                      .stream, // ส่ง Stream มาให้ ModeRunner
                                )),
                      );
                    },
                    child: Container(
                      width: 300,
                      height: 120,
                      decoration: BoxDecoration(
                        gradient: const LinearGradient(colors: [
                          Color.fromARGB(255, 235, 224, 224),
                          Color.fromARGB(255, 240, 198, 198)
                        ]),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          sizedBoxWidth(15),
                          const Text(
                            "โหมดสำหรับผู้ป่วยโรคหัวใจ",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          sizedBoxWidth(10),
                          const Icon(
                            Icons.heart_broken,
                            size: 30,
                          ),
                        ],
                      ),
                    ),
                  ),
                  sizedBoxHeight(40),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
