import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';
import 'package:hartrate/users/pages/login/login_page.dart';
import 'package:hartrate/users/pages/profiles/uploadimage.dart';
import 'package:hartrate/widget/elevatedButton.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:hartrate/widget/option.dart';
import 'package:image_picker/image_picker.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_holo_date_picker/flutter_holo_date_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Profile_page extends StatefulWidget {
  const Profile_page({super.key});

  @override
  State<Profile_page> createState() => _Profile_pageState();
}

class EditStatus {
  bool isEditing = false;
  bool isEditingName = false;
  bool isEditingAge = false;
  bool isEditingWeight = false;
  bool isEditingHeight = false;
  bool isEditingGender = false;

  void startEditing({
    bool name = false,
    bool age = false,
    bool weight = false,
    bool height = false,
    bool gender = false,
  }) {
    isEditing = true;
    isEditingName = name;
    isEditingAge = age;
    isEditingWeight = weight;
    isEditingHeight = height;
    isEditingGender = gender;
  }

  void cancelEditing() {
    isEditing = false;
    isEditingName = false;
    isEditingAge = false;
    isEditingWeight = false;
    isEditingHeight = false;
  }
}

class EditableField {
  final FocusNode focusNode;
  final String labelText;
  final String hintText;

  EditableField({
    required this.focusNode,
    required this.labelText,
    required this.hintText,
  });
}

class _Profile_pageState extends State<Profile_page> {
  User? user = FirebaseAuth.instance.currentUser;
  TextEditingController nameController = TextEditingController();
  TextEditingController ageController = TextEditingController();
  TextEditingController heightController = TextEditingController();
  TextEditingController weightController = TextEditingController();
  TextEditingController genderController = TextEditingController();

  bool dataLoaded = false;
  @override
  void initState() {
    super.initState();
    int currentYear = DateTime.now().year;
    int defaultAge = int.tryParse(ageController.text) ?? 18;
    int birthYear = currentYear - defaultAge;
    _selectedDate = DateTime(birthYear, 1, 1);
    fetchUserData();
  }

  final ImagePicker picker = ImagePicker();
  DateTime? _selectedDate;
  File? _image;
  String? profileImageUrl;
  EditStatus editStatus = EditStatus();

  EditableField nameField = EditableField(
    focusNode: FocusNode(),
    labelText: "ชื่อ",
    hintText: "ชื่อ",
  );
  EditableField heightField = EditableField(
    focusNode: FocusNode(),
    labelText: "ส่วนสูง",
    hintText: "ส่วนสูง (cm)",
  );
  EditableField weightField = EditableField(
    focusNode: FocusNode(),
    labelText: "น้ำหนัก",
    hintText: "น้ำหนัก (kg)",
  );

  int calculateAge(DateTime birthDate) {
    DateTime currentDate = DateTime.now();
    int age = 0;
    age = currentDate.year - birthDate.year;
    if (currentDate.month < birthDate.month ||
        (currentDate.month == birthDate.month &&
            currentDate.day < birthDate.day)) {
      age--;
    }
    return age;
  }

  void saveChanges() async {
    setState(() {
      editStatus.cancelEditing();
      // Save user data here
    });
    try {
      if (user != null) {
        int userAge = calculateAge(_selectedDate!);
        await FirebaseFirestore.instance
            .collection('users')
            .doc(user!.uid)
            .update({
          'name': nameController.text,
          'age': userAge,
          'height': double.parse(heightController.text),
          'weight': double.parse(weightController.text),
        });
        print("User data updated successfully");
      }
      EasyLoading.showSuccess('บันทึกข้อมูลสำเร็จ');
    } catch (e) {
      print('Error updating user data: $e');
    }
  }

  void _openUploadImage() async {
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => UploadImage(image: _image),
      ),
    );
    fetchUserData();
  }

  Future<void> fetchUserData() async {
    try {
      User? user = FirebaseAuth.instance.currentUser;
      if (user != null) {
        EasyLoading.show(status: 'กำลังโหลดข้อมูล');
        DocumentSnapshot<Map<String, dynamic>> userData =
            await FirebaseFirestore.instance
                .collection('users')
                .doc(user.uid)
                .get();
        setState(() {
          dataLoaded = true;
          nameController.text = userData['name'];
          ageController.text = userData['age'].toString();
          profileImageUrl = userData['profileImage'];
          heightController.text = userData['height'].toString();
          weightController.text = userData['weight'].toString();
          int genderValue = userData['gender'];
          genderController.text = _getGenderText(genderValue);
        });
      }
      print("success");
      EasyLoading.dismiss();
    } catch (e) {
      print('Error fetching user data ${e}');
      print("User UID: ${user?.uid}");
      EasyLoading.dismiss();
    }
  }

  String _getGenderText(int genderValue) {
    if (genderValue == 0) {
      return 'ชาย';
    } else if (genderValue == 1) {
      return 'หญิง';
    } else {
      return 'เพศอื่น ๆ';
    }
  }

  void _logoutUser(BuildContext context) async {
    // ลบข้อมูลการเข้าสู่ระบบจาก SharedPreferences
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('email');
    prefs.remove('password');

    // ทำการออกจากระบบใน FirebaseAuth
    await FirebaseAuth.instance.signOut();

    // คุณสามารถทำการ Navigate ไปหน้า Login หรือหน้าที่คุณต้องการหลังจากออกจากระบบ
    PersistentNavBarNavigator.pushNewScreen(context,
        screen: Login_Page(),
        withNavBar: false,
        pageTransitionAnimation: PageTransitionAnimation.cupertino);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(30, 80, 30, 50),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: _openUploadImage,
                child: CircleAvatar(
                  radius: 60,
                  backgroundImage: profileImageUrl != null
                      ? NetworkImage(profileImageUrl!)
                      : const AssetImage('images/profilebase.png')
                          as ImageProvider,
                ),
              ),
              const SizedBox(height: 20),
              Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      Color.fromARGB(255, 218, 224, 233),
                      Color.fromARGB(26, 207, 201, 201)
                    ]),
                    borderRadius: BorderRadius.circular(20)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    sizedBoxWidth(10),
                    Text(
                      nameField.labelText,
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                    sizedBoxWidth(62),
                    Expanded(
                      child: TextFormField(
                        readOnly: !editStatus.isEditingName,
                        controller: nameController,
                        focusNode: nameField.focusNode,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide.none,
                          ),
                          suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                editStatus.startEditing(name: true);
                                nameField.focusNode.requestFocus();
                              });
                            },
                            icon: const Icon(Icons.edit),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              sizedBoxHeight(10),
              Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      Color.fromARGB(255, 218, 224, 233),
                      Color.fromARGB(26, 207, 201, 201)
                    ]),
                    borderRadius: BorderRadius.circular(20)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    sizedBoxWidth(10),
                    const Text(
                      "อายุ",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                    sizedBoxWidth(56),
                    Expanded(
                      child: TextFormField(
                        readOnly: !editStatus.isEditingAge,
                        controller: ageController,
                        style: TextStyle(fontSize: 16),
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide.none, // เส้นขอบเมื่อไม่ได้โฟกัส
                          ),
                          hintText: 'อายุ',
                          suffixIcon: Icon(Icons.arrow_drop_down),
                        ),
                        onTap: () {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: const Text('อายุ'),
                                  content: DatePickerWidget(
                                    looping: false,
                                    firstDate: DateTime(1950, 1, 1),
                                    lastDate: DateTime.now(),
                                    initialDate: DateTime(2001, 1, 1),
                                    dateFormat: "dd/MMMM/yyyy",
                                    locale: DatePicker.localeFromString('th'),
                                    onChange:
                                        (DateTime newDate, List<int> index) {
                                      setState(() {
                                        editStatus.startEditing(age: true);
                                        _selectedDate = newDate;
                                        int userAge = calculateAge(newDate);
                                        ageController.text = userAge.toString();
                                      });
                                    },
                                  ),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () {
                                        Navigator.of(context)
                                            .pop(); // ปิด AlertDialog
                                      },
                                      child: const Text('ยกเลิก'),
                                    ),
                                    ElevatedButton(
                                      onPressed: () {
                                        setState(() {
                                          // ตอนนี้คุณมีการเปลี่ยนแปลงใน ageController แล้ว
                                          Navigator.of(context)
                                              .pop(); // ปิด AlertDialog
                                        });
                                      },
                                      child: const Text('ยืนยัน'),
                                    ),
                                  ],
                                );
                              });
                        },
                      ),
                    ),
                  ],
                ),
              ),
              sizedBoxHeight(10),
              Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      Color.fromARGB(255, 218, 224, 233),
                      Color.fromARGB(26, 207, 201, 201)
                    ]),
                    borderRadius: BorderRadius.circular(20)),
                child: Row(
                  children: [
                    sizedBoxWidth(10),
                    Text(
                      heightField.labelText,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                    sizedBoxWidth(38),
                    Expanded(
                      child: TextFormField(
                        readOnly: !editStatus.isEditingHeight,
                        controller: heightController,
                        focusNode: heightField.focusNode,
                        style: TextStyle(fontSize: 16),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide.none,
                          ),
                          hintText: heightField.hintText,
                          suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                editStatus.startEditing(height: true);
                                heightField.focusNode.requestFocus();
                              });
                            },
                            icon: const Icon(Icons.edit),
                          ),
                        ),
                        keyboardType: TextInputType.number,
                      ),
                    ),
                  ],
                ),
              ),
              sizedBoxHeight(10),
              Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      Color.fromARGB(255, 218, 224, 233),
                      Color.fromARGB(26, 207, 201, 201)
                    ]),
                    borderRadius: BorderRadius.circular(20)),
                child: Row(
                  children: [
                    sizedBoxWidth(10),
                    Text(
                      weightField.labelText,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                    sizedBoxWidth(35),
                    Expanded(
                      child: TextFormField(
                        readOnly: !editStatus.isEditingWeight,
                        controller: weightController,
                        focusNode: weightField.focusNode,
                        style: TextStyle(fontSize: 16),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide.none, // เส้นขอบเมื่อไม่ได้โฟกัส
                          ),
                          hintText: weightField.hintText,
                          suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                editStatus.startEditing(weight: true);
                                weightField.focusNode.requestFocus();
                              });
                            },
                            icon: const Icon(Icons.edit),
                          ),
                        ),
                        keyboardType: TextInputType.number,
                      ),
                    ),
                  ],
                ),
              ),
              sizedBoxHeight(10),
              Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      Color.fromARGB(255, 218, 224, 233),
                      Color.fromARGB(26, 207, 201, 201)
                    ]),
                    borderRadius: BorderRadius.circular(20)),
                child: Row(
                  children: [
                    sizedBoxWidth(10),
                    const Text(
                      "เพศ",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                    sizedBoxWidth(57),
                    Expanded(
                      child: TextFormField(
                        readOnly: true,
                        controller: genderController,
                        style: TextStyle(fontSize: 16),
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide.none, // เส้นขอบเมื่อไม่ได้โฟกัส
                          ),
                          hintText: '',
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              sizedBoxHeight(30),
              Row(
                children: [
                  sizedBoxWidth(20),
                  Buttonsave(() {
                    saveChanges();
                    fetchUserData();
                  }, fetchUserData, "บันทึก"),
                  sizedBoxWidth(30),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.redAccent,
                      fixedSize: Size(120, 50),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25),
                      ),
                    ),
                    onPressed: () {
                      _logoutUser(context);
                    },
                    child: Text(
                      'ออกจากระบบ',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
