import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:hartrate/widget/navigatorbuttom.dart';
import 'package:hartrate/widget/option.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'resetpassword_page.dart';

class Login_Page extends StatefulWidget {
  Login_Page({super.key});

  @override
  State<Login_Page> createState() => _Login_PageState();
}

class _Login_PageState extends State<Login_Page> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  bool obscureText = true;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool rememberMe = false;

  // void updateRememberMe(bool newValue) {
  //   setState(() {
  //     rememberMe = newValue;
  //   });
  // }

  Future<void> _login(BuildContext context) async {
    EasyLoading.show(status: 'กำลังโหลดข้อมูล');
    try {
      UserCredential userCredential =
          await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: emailController.text,
        password: passwordController.text,
      );

      if (userCredential.user != null) {
        // Save email and password if "Remember Me" is checked
        if (rememberMe) {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString('email', emailController.text);
          prefs.setString('password', passwordController.text);
        }
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => NavigatorButtoms()),
        );
        EasyLoading.dismiss();
      }
    } catch (e) {
      if (e.toString().contains(
          'There is no user record corresponding to this identifier. The user may have been deleted.')) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('ไม่พบอีเมล์ในระบบ'),
          ),
        );
        EasyLoading.dismiss();
      } else if (e.toString().contains(
          'The password is invalid or the user does not have a password')) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('รหัสผ่านไม่ถูกต้อง'),
          ),
        );
        EasyLoading.dismiss();
      } else if (e.toString().contains('A network error')) {
        EasyLoading.dismiss();
        AwesomeDialog(
          context: context,
          dialogType: DialogType.warning,
          borderSide: const BorderSide(color: Colors.orange),
          animType: AnimType.rightSlide,
          title: 'แจ้งเตือน',
          btnOkColor: Colors.blue,
          desc: 'โปรดเชื่อมต่อ Internet',
          btnCancelOnPress: () {},
          btnOkOnPress: () {},
        ).show();
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('เกิดข้อผิดพลาดในการเข้าสู่ระบบ'),
          ),
        );
        EasyLoading.dismiss();
      }
      print('Error: $e');
    }
  }

  void loadSavedCredentials() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? savedEmail = prefs.getString('email');
    String? savedPassword = prefs.getString('password');

    if (savedEmail != null && savedPassword != null) {
      setState(() {
        emailController.text = savedEmail;
        passwordController.text = savedPassword;
        rememberMe = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    loadSavedCredentials();
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.white],
                  stops: [1],
                  begin: AlignmentDirectional(-1, -1),
                  end: AlignmentDirectional(1, 1),
                ),
              ),
              padding: EdgeInsets.fromLTRB(0, 100, 0, 0),
              child: Column(
                children: [
                  Image.asset(
                    'images/heartlatelogoapp.png',
                    fit: BoxFit.cover,
                    width: 100,
                    height: 100,
                  ),
                  sizedBoxHeight(20),
                  Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(colors: [
                        Color.fromARGB(255, 250, 199, 122),
                        Color.fromARGB(255, 255, 181, 178)
                      ]),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(
                          left: 30, right: 30, top: 80, bottom: 40),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            TextFormFieldEmail(
                              emailController,
                              isValidEmail,
                            ),
                            sizedBoxHeight(25),
                            TextFormFieldPassword(),
                            sizedBoxHeight(20),
                            Row(
                              children: [
                                Checkbox(
                                  value: rememberMe,
                                  onChanged: (newValue) {
                                    setState(() {
                                      rememberMe = newValue!;
                                    });
                                  },
                                ),
                                Expanded(
                                  child: Text('จดจำรหัสผ่าน'),
                                ),
                                Align(
                                  alignment: Alignment.centerRight,
                                  child: TextButton(
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              ResetPasswordPage(),
                                        ),
                                      );
                                    },
                                    child: const Text('ลืมรหัสผ่าน?',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Color.fromARGB(
                                                255, 43, 57, 253))),
                                  ),
                                ),
                              ],
                            ),
                            sizedBoxHeight(20),
                            ElevatedButton(
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  _login(context);
                                }
                              },
                              style: ElevatedButton.styleFrom(
                                backgroundColor:
                                    Color.fromARGB(238, 33, 79, 163),
                                fixedSize: const Size(250, 50),
                                shape: const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(25),
                                  ),
                                ),
                              ),
                              child: Text(
                                'Login',
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              ),
                            ),
                            sizedBoxHeight(30),
                            ButtonCreateAccount(context),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  TextFormField TextFormFieldPassword() {
    return TextFormField(
      controller: passwordController,
      obscureText: obscureText,
      // enabled: !rememberMe,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white,
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            color: Color.fromARGB(255, 5, 113, 255),
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide:
              const BorderSide(color: Colors.grey), // เส้นขอบเมื่อไม่ได้โฟกัส
          borderRadius: BorderRadius.circular(10),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: const BorderSide(
              color: Colors.red), // เส้นขอบเมื่อข้อมูลไม่ถูกต้อง
          borderRadius: BorderRadius.circular(10),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            color: Colors.red,
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        hintText: 'Password',
        prefixIcon: const Icon(Icons.lock),
        suffixIcon: IconButton(
          icon: Icon(
            obscureText ? Icons.visibility_off : Icons.visibility,
          ),
          onPressed: () {
            setState(() {
              obscureText = !obscureText;
            });
          },
        ),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'โปรดกรอกรหัสผ่าน';
        }
        return null;
      },
    );
  }
}
