import 'dart:ui';
import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:hartrate/users/pages/login/login_page.dart';
import 'package:hartrate/widget/option.dart';

enum Gender {
  male,
  female,
  other,
  preferNotToSay,
}

const Map<Gender, int> genderMap = {
  Gender.male: 0,
  Gender.female: 1,
  Gender.other: 2,
  Gender.preferNotToSay: 3,
};

class Register_Page extends StatefulWidget {
  Register_Page({super.key});

  @override
  State<Register_Page> createState() => _Register_PageState();
}

class _Register_PageState extends State<Register_Page> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController ageController = TextEditingController();
  final imageController = "profilebase.png";
  final TextEditingController heightController =
      TextEditingController(text: "0");
  final TextEditingController weightController =
      TextEditingController(text: "0");
  final TextEditingController genderController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isValidated = false;
  bool obscureText = true;
  bool _acceptedTerms = false;
  int genderValue = Gender.other.index; //ดึงค่า enum

  String _getGenderText(int genderValue) {
    switch (genderValue) {
      case 0:
        return 'ชาย';
      case 1:
        return 'หญิง';
      case 2:
        return 'เพศอื่น ๆ';
      case 3:
        return 'ไม่ต้องการระบุ';
      default:
        return 'ไม่ระบุ';
    }
  }

  bool _isDataComplete() {
    return emailController.text.isNotEmpty &&
        nameController.text.isNotEmpty &&
        passwordController.text.isNotEmpty &&
        ageController.text.isNotEmpty &&
        heightController.text.isNotEmpty &&
        weightController.text.isNotEmpty;
  }

  Future<void> _register(BuildContext context) async {
    EasyLoading.show(status: 'กำลังโหลดข้อมูล');
    try {
      UserCredential userCredential =
          await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: emailController.text,
        password: passwordController.text,
      );

      if (userCredential.user != null) {
        String uid = userCredential.user!.uid;
        await FirebaseFirestore.instance.collection('users').doc(uid).set({
          'uid': uid,
          'email': emailController.text,
          'name': nameController.text,
          'age': ageController.text,
          'profileImage': null,
          'height': heightController.text,
          'weight': weightController.text,
          'Date': DateTime.now(),
          'gender': genderValue,
          // อื่น ๆ ข้อมูลที่คุณต้องการเก็บ
        });

        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => Login_Page(),
          ),
        );
        EasyLoading.showSuccess('สมัครสมาชิกสำเร็จ!');
        EasyLoading.dismiss();
      }
    } catch (e) {
      if (e.toString().contains(
          "[firebase_auth/email-already-in-use] The email address is already in use by another account.")) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('อีเมล์นี้มีในระบบแล้ว'),
          ),
        );
        EasyLoading.dismiss();
      } else {
        EasyLoading.dismiss();
      }
      EasyLoading.dismiss();
      print('Error: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('สมัครสมาชิก'),
        backgroundColor: Colors.orange,
      ),
      body: Stack(
        children: [
          Image.asset(
            'images/backgroudregister.jpg', // Replace with your background image path
            fit: BoxFit.cover,
            width: double.infinity,
            height: double.infinity,
          ),
          BackdropFilter(
            filter:
                ImageFilter.blur(sigmaX: 5, sigmaY: 5), // Adjust blur values
            child: Container(
              color: Color.fromARGB(255, 58, 55, 55)
                  .withOpacity(0.2), // Adjust color and opacity
              width: double.infinity,
              height: double.infinity,
            ),
          ),
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(30, 60, 30, 60),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    sizedBoxHeight(20),
                    TextFormFieldEmailRegister(emailController, isValidEmail),
                    sizedBoxHeight(20),
                    TextFormFieldName(nameController),
                    sizedBoxHeight(20),
                    TextFormfieldPassword(),
                    sizedBoxHeight(20),
                    Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: TextFormField(
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              hintText: 'เพศ',
                              prefixIcon: const Icon(Icons.person),
                              suffixIcon: const Icon(Icons.arrow_drop_down),
                              focusedBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                  color: Color.fromARGB(255, 5, 113, 255),
                                ),
                                borderRadius: BorderRadius.circular(20),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color:
                                        Colors.grey), // เส้นขอบเมื่อไม่ได้โฟกัส
                                borderRadius: BorderRadius.circular(20),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors
                                        .red), // เส้นขอบเมื่อข้อมูลไม่ถูกต้อง
                                borderRadius: BorderRadius.circular(20),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                  color: Colors.red,
                                ),
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'โปรดเลือกเพศ';
                              }
                              return null;
                            },
                            readOnly: true,
                            controller: genderController,
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      title: const Text('เลือกเพศ'),
                                      content: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          ListTile(
                                            title: const Text('ชาย'),
                                            onTap: () {
                                              setState(() {
                                                genderValue = Gender.male.index;
                                                genderController.text =
                                                    _getGenderText(
                                                        Gender.male.index);
                                              });
                                              Navigator.pop(context);
                                            },
                                          ),
                                          ListTile(
                                            title: const Text('หญิง'),
                                            onTap: () {
                                              setState(() {
                                                genderValue =
                                                    Gender.female.index;
                                                genderController.text =
                                                    _getGenderText(
                                                        Gender.female.index);
                                              });
                                              Navigator.pop(context);
                                            },
                                          )
                                        ],
                                      ),
                                    );
                                  });
                            },
                          ),
                        ),
                        sizedBoxWidth(16),
                        Expanded(
                          flex: 2,
                          child: TextFormField(
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              hintText: 'อายุ',
                              prefixIcon: const Icon(Icons.radar),
                              focusedBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                  color: Color.fromARGB(255, 5, 113, 255),
                                ),
                                borderRadius: BorderRadius.circular(20),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                    const BorderSide(color: Colors.grey),
                                borderRadius: BorderRadius.circular(20),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderSide: const BorderSide(color: Colors.red),
                                borderRadius: BorderRadius.circular(20),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                  color: Colors.red,
                                ),
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                            keyboardType:
                                const TextInputType.numberWithOptions(),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'โปรดใส่อายุ';
                              }

                              return null;
                            },
                            controller: ageController,
                          ),
                        ),
                      ],
                    ),
                    sizedBoxHeight(20),
                    Column(
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Checkbox(
                              value: _acceptedTerms,
                              checkColor: Colors.white,
                              activeColor: Colors.orange,
                              onChanged: (value) {
                                setState(() {
                                  _acceptedTerms = value!;
                                  isValidated = true;
                                });
                              },
                            ),
                            const Expanded(
                              child: Text(
                                'I have read and agree to the Terms of Service privacy policy',
                                textAlign: TextAlign.justify,
                                overflow: TextOverflow.visible,
                                style: TextStyle(
                                    fontSize: 11,
                                    color: Color.fromARGB(255, 0, 0, 0)),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    sizedBoxHeight(20),
                    ElevatedButton(
                      onPressed: () {
                        setState(() {
                          isValidated = true;
                        });

                        if (_formKey.currentState!.validate() &&
                            _acceptedTerms &&
                            _isDataComplete()) {
                          _register(context);
                        } else if (!_acceptedTerms) {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              elevation: 0,
                              behavior: SnackBarBehavior.floating,
                              backgroundColor:
                                  const Color.fromARGB(0, 170, 32, 32),
                              content: AwesomeSnackbarContent(
                                title: "แจ้งเตือน!",
                                message: "กรุณายอมรับเงื่อนไข",
                                contentType: ContentType.warning,
                              ),
                            ),
                          );
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              elevation: 0,
                              behavior: SnackBarBehavior.floating,
                              backgroundColor:
                                  const Color.fromARGB(0, 170, 32, 32),
                              content: AwesomeSnackbarContent(
                                title: "แจ้งเตือน!",
                                message: "กรุณากรอกข้อมูลให้ครบ",
                                contentType: ContentType.warning,
                              ),
                            ),
                          );
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor:
                            isValidated && _acceptedTerms && _isDataComplete()
                                ? Colors.orange
                                : const Color.fromARGB(255, 177, 175, 175),
                        fixedSize: const Size(250, 50),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(25),
                          ),
                        ),
                      ),
                      child: const Text(
                        'Sign up',
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w500),
                      ),
                    ),
                    const SizedBox(height: 40),
                    ElevatedButton(
                      onPressed: () {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Login_Page(),
                          ),
                        );
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: const Color.fromARGB(255, 255, 102, 0),
                        fixedSize: const Size(250, 50),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(25),
                          ),
                        ),
                      ),
                      child: const Text(
                        'Already have an account?',
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w500),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  TextFormField TextFormfieldPassword() {
    return TextFormField(
      controller: passwordController,
      obscureText: obscureText,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white,
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            color: Color.fromARGB(255, 5, 113, 255),
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide:
              const BorderSide(color: Colors.grey), // เส้นขอบเมื่อไม่ได้โฟกัส
          borderRadius: BorderRadius.circular(10),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: const BorderSide(
              color: Colors.red), // เส้นขอบเมื่อข้อมูลไม่ถูกต้อง
          borderRadius: BorderRadius.circular(10),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            color: Colors.red,
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        hintText: 'Password',
        prefixIcon: const Icon(Icons.lock),
        suffixIcon: IconButton(
          icon: Icon(
            obscureText ? Icons.visibility_off : Icons.visibility,
          ),
          onPressed: () {
            setState(() {
              obscureText = !obscureText;
            });
          },
        ),
      ),
      validator: ((value) {
        if (value!.isEmpty) {
          return 'กรุณาใส่รหัสผ่าน';
        }
        if (value.length < 6) {
          return 'Password must be at least 6 characters';
        }
        return null;
      }),
    );
  }
}
