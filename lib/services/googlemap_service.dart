import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
// import 'package:text_form_field_wrapper/text_form_field_wrapper.dart';

class MyWidget extends StatefulWidget {
  const MyWidget({super.key});

  @override
  State<MyWidget> createState() => _MyWidgetState();
}

Completer<GoogleMapController> _controller = Completer();

class _MyWidgetState extends State<MyWidget> {
  LocationData? currentLocation;

  @override
  void initState() {
    super.initState();
    getCurrentLocation();
  }

  Future<void> getCurrentLocation() async {
    try {
      final location = Location();
      currentLocation = await location.getLocation();
      if (currentLocation != null) {
        final GoogleMapController controller = await _controller.future;
        controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
            target:
                LatLng(currentLocation!.latitude!, currentLocation!.longitude!),
            zoom: 16.0)));
      }
    } catch (e) {
      print("เกิดข้อผิดพลาดในการรับตำแหน่ง: $e");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mode patients'),
      ),
      body: Container(
        width: double.infinity,
        margin: EdgeInsets.all(20),
        child: Column(
          children: [
            Expanded(
              child: (TextFormField()),
            )
          ],
        ),
      ),
    );
  }
}
