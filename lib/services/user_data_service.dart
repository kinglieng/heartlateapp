import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class UserDataService {
  static Future<Map<String, dynamic>> fetchUserData() async {
    try {
      final User? user = FirebaseAuth.instance.currentUser;
      if (user != null) {
        final DocumentSnapshot<Map<String, dynamic>> userData =
            await FirebaseFirestore.instance
                .collection('users')
                .doc(user.uid)
                .get();
        return {
          'name': userData['name'],
          'age': userData['age'].toString(),
          'height': userData['height'].toString(),
          'weight': userData['weight'].toString(),
          'gender': userData['gender'],
        };
      } else {
        return {};
      }
    } catch (e) {
      print('Error fetching user data ${e}');
      return {};
    }
  }
}

class PatientDataService {
  static Future<List<Map<String, dynamic>>> fetchPatientHistoryData() async {
    try {
      final User? user = FirebaseAuth.instance.currentUser;
      final QuerySnapshot<Map<String, dynamic>> querySnapshot =
          await FirebaseFirestore.instance
              .collection('users')
              .doc(user!.uid)
              .collection('patient_history')
              .get();

      final List<Map<String, dynamic>> dataList = querySnapshot.docs
          .map((QueryDocumentSnapshot<Map<String, dynamic>> document) =>
              document.data())
          .toList();

      return dataList;
    } catch (e) {
      print('Error fetching patient history data: $e');
      return [];
    }
  }
}
