import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class BpmService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  String statusbpm = 'green';

  void startBpmRecording(int bpmValue, int maxHR) {
    if (bpmValue >= 0.9 * maxHR) {
      saveBpmToFirestore(bpmValue, 'red');
    } else if (bpmValue >= 0.8 * maxHR) {
      saveBpmToFirestore(bpmValue, 'orange');
    } else if (bpmValue >= 0.7 * maxHR) {
      saveBpmToFirestore(bpmValue, 'yellow');
    } else if (bpmValue >= 0.6 * maxHR) {
      saveBpmToFirestore(bpmValue, 'blue');
    } else {
      saveBpmToFirestore(bpmValue, 'green');
    }
  }

  Future<void> saveBpmToFirestore(int bpmValue, String status) async {
    try {
      User? user = _auth.currentUser;

      // ตรวจสอบว่าผู้ใช้ล็อกอินแล้ว
      if (user != null) {
        // สร้างเอกสารใหม่ใน Firestore เพื่อเก็บค่า BPM
        await _firestore
            .collection('users')
            .doc(user.uid)
            .collection('bpm_data')
            .add({
          'bpm_value': bpmValue,
          'bpm_status': status,
          'timestamp': FieldValue.serverTimestamp(),
        });

        // บันทึกข้อมูลสำเร็จ
        print('บันทึกค่า BPM สำเร็จ: $bpmValue');
      } else {
        // ผู้ใช้ยังไม่ล็อกอิน ไม่สามารถบันทึกข้อมูลได้
        print('ผู้ใช้ยังไม่ล็อกอิน');
      }
    } catch (e) {
      // เกิดข้อผิดพลาดในการบันทึกข้อมูล
      print('เกิดข้อผิดพลาดในการบันทึกค่า BPM: $e');
    }
  }
}
