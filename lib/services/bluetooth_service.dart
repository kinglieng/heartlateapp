import 'dart:async';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_reactive_ble/flutter_reactive_ble.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:permission_handler/permission_handler.dart';

class BluetoothServiceBle {
  final FlutterReactiveBle flutterReactiveBle = FlutterReactiveBle();
  StreamSubscription<List<int>>? heartRateSubscription;
  final deviceId = dotenv.env["deviceId"];
  Future<void> requestBluetoothPermission() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.location,
      Permission.storage,
      Permission.bluetooth,
    ].request();

    if (statuses[Permission.bluetooth]!.isDenied) {
      Fluttertoast.showToast(
        msg: "กรุณาเปิด Bluetooth เพื่อใช้งานแอป",
        toastLength: Toast.LENGTH_LONG,
      );
    } else if (statuses[Permission.bluetooth]!.isPermanentlyDenied) {
      // แจ้งเตือนให้ไปเปิด Bluetooth ในการตั้งค่าอุปกรณ์
      Fluttertoast.showToast(
        msg: "กรุณาเปิด Bluetooth ในการตั้งค่าอุปกรณ์เพื่อใช้งานแอป",
        toastLength: Toast.LENGTH_LONG,
      );
    }
  }

  Future<void> connectToDevice(
      StreamController<int> heartRateController) async {
    try {
      await requestBluetoothPermission();

      final serviceId = Uuid.parse("0000180d-0000-1000-8000-00805f9b34fb");
      final characteristicId =
          Uuid.parse("00002a37-0000-1000-8000-00805f9b34fb");
      final characteristic = QualifiedCharacteristic(
        characteristicId: characteristicId,
        serviceId: serviceId,
        deviceId: deviceId!,
      );

      heartRateSubscription =
          subscribeToHeartRate(characteristic, heartRateController);
      print("test = +$heartRateSubscription");
    } on Error catch (e) {
      print("Error $e");
    }
  }

  StreamSubscription<List<int>> subscribeToHeartRate(
      QualifiedCharacteristic characteristic,
      StreamController<int> heartRateController) {
    return flutterReactiveBle
        .subscribeToCharacteristic(characteristic)
        .listen((data) {
      final newHeartRate = _parseHeartRate(data);
      heartRateController.add(newHeartRate);
      print("Heart Rate: $newHeartRate bpm");
      // You can handle the heart rate data here, or pass it to a stream controller.
    });
  }

  void disconnect() {
    heartRateSubscription?.cancel();
  }

  int _parseHeartRate(List<int> value) {
    if ((value[0] & 0x01) == 0) {
      return value[1];
    } else {
      return (value[1] << 8) + value[2];
    }
  }

  // Function to check the battery status
  Future<int?> checkBatteryStatus() async {
    final serviceId = Uuid.parse("0000180d-0000-1000-8000-00805f9b34fb");
    final characteristicId = Uuid.parse("00002a19-0000-1000-8000-00805f9b34fb");
    final characteristic = QualifiedCharacteristic(
      characteristicId: characteristicId,
      serviceId: serviceId,
      deviceId: deviceId!,
    );

    try {
      final data = await flutterReactiveBle.readCharacteristic(characteristic);
      //final batteryStatus = _parseBatteryStatus(data);
      print("Battery Status: $data");
      return data[0];
    } catch (e) {
      print("Error reading battery status: $e");
      return null;
    }
  }
}
