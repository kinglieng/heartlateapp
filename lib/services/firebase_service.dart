import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class FirebaseService {
  final User? user = FirebaseAuth.instance.currentUser;

  final CollectionReference bpmCollection =
      FirebaseFirestore.instance.collection('users');
  Future<void> saveBpmToFirebase(int bpm) async {
    if (user != null) {
      final bpmCollectionForUser =
          bpmCollection.doc(user!.uid).collection('bpm');

      await bpmCollectionForUser.add({
        'bpm': bpm,
        'timestamp': Timestamp.fromDate(
            DateTime.now()), // ใช้ Timestamp จาก cloud_firestore
      });
    } else {
      throw Exception("User not logged in.");
    }
  }
}
