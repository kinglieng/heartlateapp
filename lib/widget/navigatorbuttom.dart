import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';
import 'package:hartrate/users/pages/setting/notification.dart';
import 'package:hartrate/users/pages/Home.dart';
import 'package:hartrate/users/pages/pulse/graphdata_page.dart';
import 'package:hartrate/users/pages/pulse/modepatients.dart';
import 'package:hartrate/users/pages/pulse/moderunning.dart';
import 'package:hartrate/users/pages/moniter_page.dart';
import 'package:hartrate/users/pages/profiles/profile_page.dart';

import '../users/pages/setting/linelogin.dart';

class NavigatorButtoms extends StatefulWidget {
  NavigatorButtoms({super.key});

  @override
  State<NavigatorButtoms> createState() => _NavigatorButtomsState();
}

class _NavigatorButtomsState extends State<NavigatorButtoms> {
  @override
  Widget build(BuildContext context) {
    PersistentTabController _controller;
    _controller = PersistentTabController(initialIndex: 0);

    List<Widget> _buildScreens() {
      return [
        const Home(),
        const GraphData(),
        LineLoginPage(),
        const Moniter_page(),
        const Profile_page(),
      ];
    }

    List<PersistentBottomNavBarItem> _navBarsItems() {
      return [
        PersistentBottomNavBarItem(
          icon: Icon(Icons.home),
          title: ("Messure"),
          activeColorPrimary: Colors.orangeAccent,
          inactiveColorPrimary: Colors.grey,
        ),
        PersistentBottomNavBarItem(
          icon: Icon(Icons.data_exploration),
          title: ("ข้อมูล"),
          activeColorPrimary: Colors.orangeAccent,
          inactiveColorPrimary: Colors.grey,
        ),
        PersistentBottomNavBarItem(
          icon: Icon(Icons.add_alert),
          title: ("แจ้งเตือน"),
          activeColorPrimary: Colors.orangeAccent,
          inactiveColorPrimary: Colors.grey,
        ),
        PersistentBottomNavBarItem(
          icon: Icon(Icons.settings),
          title: ("setting"),
          activeColorPrimary: Colors.orangeAccent,
          inactiveColorPrimary: Colors.grey,
        ),
        PersistentBottomNavBarItem(
          icon: Icon(Icons.person),
          title: ("profile"),
          activeColorPrimary: Colors.orangeAccent,
          inactiveColorPrimary: Colors.grey,
        ),
      ];
    }

    return PersistentTabView(
      context,
      controller: _controller,
      screens: _buildScreens(),
      items: _navBarsItems(),
      confineInSafeArea: true,
      backgroundColor: Color.fromARGB(255, 64, 68, 68),
      handleAndroidBackButtonPress: true,
      resizeToAvoidBottomInset: true,
      stateManagement: true,
      hideNavigationBarWhenKeyboardShows: true,
      decoration: NavBarDecoration(
        borderRadius: BorderRadius.circular(0),
        colorBehindNavBar: Colors.white,
      ),
      popAllScreensOnTapOfSelectedTab: true,
      popActionScreens: PopActionScreensType.all,
      itemAnimationProperties: ItemAnimationProperties(
        curve: Curves.ease,
        duration: Duration(microseconds: 200),
      ),
      screenTransitionAnimation: ScreenTransitionAnimation(
        animateTabTransition: true,
        curve: Curves.ease,
        duration: Duration(milliseconds: 200),
      ),
      navBarStyle: NavBarStyle.style6,
    );
  }
}
