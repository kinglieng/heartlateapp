import 'package:flutter/material.dart';

TextFormField textFormfieldPassword(namecontroller, String hintText) {
  return TextFormField(
    controller: namecontroller,
    decoration: InputDecoration(
      focusedBorder: OutlineInputBorder(
        borderSide: const BorderSide(
          color: Color.fromARGB(255, 5, 113, 255),
        ),
        borderRadius: BorderRadius.circular(10),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide:
            const BorderSide(color: Colors.grey), // เส้นขอบเมื่อไม่ได้โฟกัส
        borderRadius: BorderRadius.circular(10),
      ),
      errorBorder: OutlineInputBorder(
        borderSide:
            const BorderSide(color: Colors.red), // เส้นขอบเมื่อข้อมูลไม่ถูกต้อง
        borderRadius: BorderRadius.circular(10),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderSide: const BorderSide(
          color: Colors.red,
        ),
        borderRadius: BorderRadius.circular(10),
      ),
      hintText: hintText,
      prefixIcon: const Icon(Icons.person),
    ),
  );
}
