import 'package:flutter/material.dart';

class TextFormFieldPassword extends StatelessWidget {
  final TextEditingController passwordController;
  late final bool obscureText;
  TextFormFieldPassword(this.passwordController, this.obscureText);
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: passwordController,
      obscureText: obscureText,
      decoration: InputDecoration(
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            color: Color.fromARGB(255, 5, 113, 255),
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide:
              const BorderSide(color: Colors.grey), // เส้นขอบเมื่อไม่ได้โฟกัส
          borderRadius: BorderRadius.circular(10),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: const BorderSide(
              color: Colors.red), // เส้นขอบเมื่อข้อมูลไม่ถูกต้อง
          borderRadius: BorderRadius.circular(10),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            color: Colors.red,
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        hintText: 'Password',
        prefixIcon: const Icon(Icons.lock),
        suffixIcon: IconButton(
            icon: Icon(
              obscureText ? Icons.visibility_off : Icons.visibility,
            ),
            onPressed: () {
              obscureText = !obscureText;
            }),
      ),
      validator: ((value) {
        if (value!.isEmpty) {
          return 'กรุณาใส่รหัสผ่าน';
        }
        if (value.length < 6) {
          return 'Password must be at least 6 characters';
        }
        return null;
      }),
    );
  }
}
