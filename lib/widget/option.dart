import 'package:flutter/material.dart';

import '../users/pages/login/register_page.dart';

SizedBox sizedBoxHeight(double height) {
  return SizedBox(
    height: height,
  );
}

SizedBox sizedBoxWidth(double width) {
  return SizedBox(
    width: width,
  );
}

bool isValidEmail(String email) {
  final RegExp emailRegExp = RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$');
  return emailRegExp.hasMatch(email);
}

TextFormField TextFormFieldEmail(
  email,
  isValidEmail,
) {
  return TextFormField(
    controller: email,
    decoration: InputDecoration(
      filled: true,
      fillColor: Colors.white,
      focusedBorder: OutlineInputBorder(
        borderSide: const BorderSide(
          color: Color.fromARGB(255, 5, 113, 255),
        ),
        borderRadius: BorderRadius.circular(10),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide:
            const BorderSide(color: Colors.grey), // เส้นขอบเมื่อไม่ได้โฟกัส
        borderRadius: BorderRadius.circular(10),
      ),
      errorBorder: OutlineInputBorder(
        borderSide:
            const BorderSide(color: Colors.red), // เส้นขอบเมื่อข้อมูลไม่ถูกต้อง
        borderRadius: BorderRadius.circular(10),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderSide: const BorderSide(
          color: Colors.red,
        ),
        borderRadius: BorderRadius.circular(10),
      ),
      hintText: 'Email',
      prefixIcon: const Icon(Icons.email),
    ),
    keyboardType: TextInputType.emailAddress,
    validator: (value) {
      if (value == null || value.isEmpty) {
        return 'โปรดกรอกอีเมล์';
      } else if (!isValidEmail(value)) {
        return 'กรุณาใส่อีเมล์ที่ถูกต้อง';
      }
      return null;
    },
  );
}

TextFormField TextFormFieldEmailRegister(
  email,
  isValidEmail,
) {
  return TextFormField(
    controller: email,
    decoration: InputDecoration(
      filled: true,
      fillColor: Colors.white,
      focusedBorder: OutlineInputBorder(
        borderSide: const BorderSide(
          color: Color.fromARGB(255, 5, 113, 255),
        ),
        borderRadius: BorderRadius.circular(10),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide:
            const BorderSide(color: Colors.grey), // เส้นขอบเมื่อไม่ได้โฟกัส
        borderRadius: BorderRadius.circular(10),
      ),
      errorBorder: OutlineInputBorder(
        borderSide:
            const BorderSide(color: Colors.red), // เส้นขอบเมื่อข้อมูลไม่ถูกต้อง
        borderRadius: BorderRadius.circular(10),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderSide: const BorderSide(
          color: Colors.red,
        ),
        borderRadius: BorderRadius.circular(10),
      ),
      hintText: 'Email',
      prefixIcon: const Icon(Icons.email),
    ),
    keyboardType: TextInputType.emailAddress,
    validator: (value) {
      if (value == null || value.isEmpty) {
        return 'โปรดกรอกอีเมล์';
      } else if (!isValidEmail(value)) {
        return 'กรุณาใส่อีเมล์ที่ถูกต้อง';
      }
      return null;
    },
  );
}

TextFormField TextFormFieldName(name) {
  return TextFormField(
    controller: name,
    decoration: InputDecoration(
      filled: true,
      fillColor: Colors.white,
      focusedBorder: OutlineInputBorder(
        borderSide: const BorderSide(
          color: Color.fromARGB(255, 5, 113, 255),
        ),
        borderRadius: BorderRadius.circular(10),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide:
            const BorderSide(color: Colors.grey), // เส้นขอบเมื่อไม่ได้โฟกัส
        borderRadius: BorderRadius.circular(10),
      ),
      errorBorder: OutlineInputBorder(
        borderSide:
            const BorderSide(color: Colors.red), // เส้นขอบเมื่อข้อมูลไม่ถูกต้อง
        borderRadius: BorderRadius.circular(10),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderSide: const BorderSide(
          color: Colors.red,
        ),
        borderRadius: BorderRadius.circular(10),
      ),
      hintText: 'Name',
      prefixIcon: const Icon(Icons.person),
    ),
    validator: ((value) {
      if (value!.isEmpty) {
        return 'กรุณาใส่ชื่อ';
      }
      return null;
    }),
  );
}

ElevatedButton ButtonCreateAccount(BuildContext context) {
  return ElevatedButton(
    onPressed: () {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => Register_Page(),
        ),
      );
    },
    style: ElevatedButton.styleFrom(
      backgroundColor: Color.fromARGB(226, 255, 76, 76),
      fixedSize: const Size(250, 50),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(25),
        ),
      ),
    ),
    child: Text(
      'Create New Account',
      style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
    ),
  );
}
