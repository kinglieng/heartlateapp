import 'package:flutter/material.dart';

ElevatedButton Buttonsave(saveChanges, fetchUserData, name) {
  return ElevatedButton(
    onPressed: () async {
      saveChanges();
      await fetchUserData();
    },
    style: ElevatedButton.styleFrom(
      backgroundColor: Colors.orangeAccent,
      fixedSize: Size(120, 50),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25),
      ),
    ),
    child: Text(
      '${name}',
      style: TextStyle(fontWeight: FontWeight.bold),
    ),
  );
}
