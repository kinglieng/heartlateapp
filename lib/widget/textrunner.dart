import 'package:flutter/material.dart';

Text TextMaxHeartRate() {
  return Text(
      'Max Heart Rate คือ ค่าการเต้นสูงสุดของหัวใจ มีไว้เพื่อเป็นแนวทางในการฝึกซ้อมการออกกำลังกายแบบ aerobic activity โดยการจำกัดขอบเขตการออกกำลังกายช่วยลำดับขั้นตอนการฝึกซ้อมจากความหนักน้อย(low intensity) ไปสู่การออกกำลังกายที่ความหนักมากขึ้น(high intensity)');
}

Text TextModeMHR() {
  return Text(
    'ออกกำลังกายเพื่อควบคุมน้ำหนักสัดส่วน (Lose weight) ควรให้อยู่ในโซน 1-2 \n' +
        'ออกกำลังกายเพื่อเพิ่มความฟิตร่างกาย (Improve Fitness) เพื่อการแข่งกีฬาทั่วไป เพิ่ม performence ก็ให้อยู่ในโซน 2-4' +
        'ออกกำลังกายเพื่อฝึกเพื่อการแข่งขัน หรือนักกีฬา ก็ควรมีการฝึกให้ขึ้นไปแตะโซน 4-5 แนะนำว่าควรมีโค้ชหรือผู้ดูแลอย่างใกล้ชิด',
    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w300),
  );
}
